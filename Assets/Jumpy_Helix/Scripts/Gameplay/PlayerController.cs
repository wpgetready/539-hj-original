﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OnefallGames;


public enum PlayerState
{
    Prepare,
    Living,
    Pause,
    PassLevel,
    Die,
}

/// <summary>
/// Trying making documentation to understand what is doing what.
/// </summary>
public class PlayerController : MonoBehaviour {
    public float radiusCollider = 0.4f;
    public LayerMask layerCollision;

    public static PlayerController Instance { private set; get; }
    public static event System.Action<PlayerState> PlayerStateChanged = delegate { };

    public PlayerState PlayerState
    {
        get
        {
            return playerState;
        }

        private set
        {
            if (value != playerState)
            {
                value = playerState;
                PlayerStateChanged(playerState);
            }
        }
    }


    private PlayerState playerState = PlayerState.Die;


    [Header("Player Config")]
    [SerializeField] private float jumpVelocity = 12;
    [SerializeField] private float fallingSpeed = -25;
    [SerializeField] private float minScale = 0.8f;
    [SerializeField] private float maxScale = 1.2f;
    [SerializeField] private float scalingFactor = 2;

    [Header("Player References")]
    [SerializeField] private HelixDetector helixDetector;
    [SerializeField] private ParticleSystem ballExplode;
    [SerializeField] private MeshRenderer meshRender;

    public float TargetY { private set; get; }

    private RaycastHit hit;
    private Vector3 originalScale = Vector3.zero;
    private float currentJumpVelocity = 0;
    private void OnEnable()
    {
        GameManager.GameStateChanged += GameManager_GameStateChanged;
    }
    private void OnDisable()
    {
        GameManager.GameStateChanged -= GameManager_GameStateChanged;
    }

    private void GameManager_GameStateChanged(GameState obj)
    {
        if (obj == GameState.Playing)
        {
            PlayerLiving();
        }
        else if (obj == GameState.Pause)
        {
            PlayerPause();
        }
    }



    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
    }

    void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }



    void Start () {

        //Fire event
        PlayerState = PlayerState.Prepare;
        playerState = PlayerState.Prepare;

        //Add another actions here
        currentJumpVelocity = jumpVelocity;
        TargetY = transform.position.y;  //TargetY seems to be needed to calculate camera position, pay attention on CameraController->Update
        originalScale = transform.localScale;
        ballExplode.gameObject.SetActive(false);
    }

    //Main loop
    void Update () {

        if (playerState == PlayerState.Living || playerState == PlayerState.PassLevel)
        {
            transform.position = transform.position + Vector3.up * (currentJumpVelocity * Time.deltaTime + fallingSpeed * Time.deltaTime * Time.deltaTime / 2);

            //currentJumpVelocity:if it is negative the ball is falling, if it is possitve is ascending. Zero means the ball reach top at top of the jump
            if (currentJumpVelocity < fallingSpeed)
                currentJumpVelocity = fallingSpeed; //Don't accelerate more than allowed. (fallingSpeed is negative, meaning vector is going down.
            else
                currentJumpVelocity = currentJumpVelocity + fallingSpeed * Time.deltaTime; //If not, keep accelerating until reach falling speed.

            if (currentJumpVelocity > 0) //If ball is going up....
            {
                Vector3 scale = transform.localScale; //scale ball in x axis but not too much.
                if (scale.x < maxScale)
                {
                    scale.x += scalingFactor * Time.deltaTime;
                }
                else
                    scale.x = maxScale;
                transform.localScale = scale;
            }
            else //notice this is only if the ball is going down, nothing happens if ball is ascending.
            {
                Vector3 scale = transform.localScale;
                if (scale.x > minScale)
                {
                    scale.x -= scalingFactor * Time.deltaTime; //current scale should be [0.8,1.2] 
                }
                else
                    scale.x = minScale;
                transform.localScale = scale;

                if (transform.position.y < TargetY)
                {
                    TargetY = transform.position.y;
                }

                //Check colliding
                //New way of checking using overlapSphere
                Collider[] colliders = Physics.OverlapSphere(transform.position, radiusCollider, layerCollision);
                



                //IMPORTANT: hit is the result of the RayCast 
                //If Raycast return true, we collided with something, otherwise return false.
                //The object we collided we can find it in hit.collider (hit is the type RaycastHit and it contains a lot of information)
                // Best raycast explanation so far: https://www.youtube.com/watch?v=EINgIoTG8D4
                if (colliders.Length>0)
                { //in hit we return where is the point where the cast hit with the plane. 
                    currentJumpVelocity = jumpVelocity; //remember currenty velocity

                    float spPosY = colliders[0].transform.position.y + colliders[0].GetComponent<MeshRenderer>().bounds.size.y / 2f + 0.01f;

                    if (colliders[0].CompareTag("Respawn")) //El mesh de la ultima pieza tiene el pivote justo arriba.
                    {
                        spPosY = colliders[0].transform.position.y + 0.01f;
                    }

                    Vector3 pointCollision = new Vector3(transform.position.x, spPosY, transform.position.z);
                    TargetY = pointCollision.y;

                    GameManager.Instance.CreateBallSplat(pointCollision, meshRender.material.color, colliders[0].transform);
                    GameManager.Instance.PlaySplatShatter(pointCollision);

                    if (playerState == PlayerState.Living)
                    {
                        for (int i=0;i<colliders.Length;i++)
                        {
                            //If the object is the bottom pillar, then we won this stage
                            if (colliders[i].CompareTag("Respawn")) //Hit the bottom pillar -> win the level
                            {
                                //We display a fading animation to make understand the player has won.
                                Vector3 fadingHelixPos = pointCollision + Vector3.down * 0.05f;
                                GameManager.Instance.CreateFadingHelix(fadingHelixPos);
                                PlayerPassLevel();
                                GameManager.Instance.PassLevel(); //and we activate passing level
                            }
                            else
                            {
                                //Ok we are not in the final level, what should we do then?
                                if (helixDetector.PassedCount >= GameManager.Instance.PassedCountForBreakHelix) //Check if the counter level is high enough for playing breaking level animation
                                {
                                    //Yeah is high enough, play a sound and a animation.
                                    SoundManager.Instance.PlaySound(SoundManager.Instance.breakPieces);
                                    colliders[i].transform.parent.GetComponent<HelixController>().ShatterAllPieces();
                                }
                                else //nope, let's check if we collided with a mortal floor..
                                {
                                    if (colliders[i].CompareTag("Finish"))//Hit dead piece -> game over
                                    { //We hit with a mortal floor, let's die shall we?
                                        PlayerDie(); //change some states and make the ball explode

                                        if (GameManager.Instance.IsRevived)
                                        {
                                            GameManager.Instance.GameOver();
                                        }
                                        else
                                        {
                                            if (AdManager.Instance.IsRewardedVideoAdReady())
                                            {
                                                GameManager.Instance.Revive();
                                            }
                                            else
                                            {
                                                GameManager.Instance.GameOver();
                                            }
                                        }
                                    }
                                    else //We didn't die, just rebound, play the audio
                                    {
                                        SoundManager.Instance.PlaySound(SoundManager.Instance.bounce);
                                    }
                                }
                            }

                            helixDetector.ResetPassedCount(); //the ball rebound in the floor, reset the counter.

                        }
                    }
                }
            }
        }      
	}

    private void PlayerLiving()
    {
        //Fire event
        PlayerState = PlayerState.Living;
        playerState = PlayerState.Living;

        //Add another actions here
        meshRender.enabled = true;
        if (GameManager.Instance.IsRevived)
        {
            currentJumpVelocity = jumpVelocity;
        }
    }

    private void PlayerPause()
    {
        //Fire event
        PlayerState = PlayerState.Pause;
        playerState = PlayerState.Pause;

        //Add another actions here
    }

    public void PlayerDie()
    {
        //Fire event
        PlayerState = PlayerState.Die;
        playerState = PlayerState.Die;

        //Add another actions here
        SoundManager.Instance.PlaySound(SoundManager.Instance.playerExplode);
        meshRender.enabled = false;
        transform.localScale = originalScale;
        StartCoroutine(PlayBallExplode());
    }

    private void PlayerPassLevel()
    {
        //Fire event
        PlayerState = PlayerState.PassLevel;
        playerState = PlayerState.PassLevel;

        //Add another actions here

    }


    //Play ball explode particle
    private IEnumerator PlayBallExplode()
    {
        ballExplode.transform.position = transform.position;
        ballExplode.gameObject.SetActive(true);
        ballExplode.Play();
        yield return new WaitForSeconds(ballExplode.main.startLifetimeMultiplier);
        ballExplode.gameObject.SetActive(false);
    }



    /// <summary>
    /// Set color for this ball (player)
    /// </summary>
    /// <param name="color"></param>
    public void SetBallColor(Color color)
    {
        meshRender.sharedMaterial.color = color;
    }
}
