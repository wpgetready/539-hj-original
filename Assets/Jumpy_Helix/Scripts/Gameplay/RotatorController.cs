﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorController : MonoBehaviour {

    [Header("Rotator Config")]
    [SerializeField] private float rotatingSpeed = 50f;


    private void Update()
    {
        if (GameManager.Instance.GameState == GameState.Playing)
        {
            if (Input.GetMouseButton(0) && GameManager.Instance.IsFinishedFading)
            {
                float x = Camera.main.ScreenToViewportPoint(Input.mousePosition).x;
                if (x <= 0.5f) //Touch left
                {
                    transform.eulerAngles += Vector3.up * rotatingSpeed * Time.deltaTime;
                }
                else //Touch right
                {
                    transform.eulerAngles += Vector3.down * rotatingSpeed * Time.deltaTime;
                }
            }
        }
    }

}
